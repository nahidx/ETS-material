# ETS-material
Material - pdf,mp3 - audio for GRE and TOEFL examination practice

## Directory structure 
It is divided into 2 sections - individual directories catering to GRE and TOEFL examination material.
1. GRE material
  * Books
        1. 5lb 
        2. Magoosh
        3. Nova
        4. ETS Official GUide to GRE 2nd Edition
        5. Gruber
        6. Barron's GRE 12th Edition
        7. Princeton Words
        8. Princeton Review - Verbal Workout
        9. RS Agarwal Quantitative Aptitude 
        10. Manhattan GRE 2nd Edition
        11. GRE Practice Test
        12. BigBook
        13. Cracking the GRE 2013 Edition
  * Series
        1. 501 Series (includes Reading Comprehension, Sentence Completion, Quantitative exams)
        2. 800 Series (Verbal tests)
  * Essay preparation, Math Review books
  * Word Power Made Easy solution
  * Bunch of Word lists - 
        1. Manhattan
        2. Barron
        3. Kaplan
        4. 840
        5. 1853
        6. 3000

2. Toefl
  * Knowledge Bank - Barron's, Longman book, Toefl Tips, Test planning, Rubrics
  * Print - Delta book sections
  * Test - Barrons, Cambridge and Official Toefl
  * Toefl CP - Section wise in-depth explanation along with samples


#### Every one is welcome to add files and make this universally available for anyone willing to ace GRE, TOEFL
In order to add files, following methods can be used - 

1. Manual method
  1. Upload the file to Google Drive and 
  2. share the link (URL)
    - In this repository as a comment, issue
    - Via email to - chai.bapat@gmail.com
2. Git method
  1. Clone this Repository
  2. Add the files
  3. Send a Merge Request
